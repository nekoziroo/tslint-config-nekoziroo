import { Configuration } from 'tslint';

const functionality: Configuration.RawRulesConfig = {
  'await-promise': true,
  ban: {
    options: [true, ['alert']],
    severity: 'warning',
  },
  'ban-comma-operator': true,
  curly: [true, 'ignore-same-line'],
  forin: true,
  'import-blacklist': false,
  'label-position': true,
  'no-arg': true,
  'no-bitwise': false,
  'no-conditional-assignment': true,
  'no-console': {
    severity: 'warning',
  },
  'no-construct': true,
  'no-debugger': true,
  'no-duplicate-super': true,
  'no-duplicate-switch-case': true,
  'no-duplicate-variable': [true, 'check-parameters'],
  'no-dynamic-delete': true,
  'no-empty': true,
  'no-eval': true,
  'no-floating-promises': true,
  'no-for-in-array': true,
  'no-implicit-dependencies': [true, 'dev'],
  'no-inferred-empty-object-type': true,
  'no-invalid-template-strings': true,
  'no-invalid-this': [true, 'check-function-in-method'],
  'no-misused-new': true,
  'no-null-keyword': false,
  'no-object-literal-type-assertion': true,
  'no-return-await': true,
  'no-shadowed-variable': true,
  'no-sparse-arrays': true,
  'no-string-literal': true,
  'no-string-throw': true,
  'no-submodule-imports': true,
  'no-switch-case-fall-through': true,
  'no-this-assignment': true,
  'no-unbound-method': true,
  'no-unnecessary-class': [true, 'allow-static-only'],
  'no-unsafe-any': true,
  'no-unsafe-finally': true,
  'no-unused-expression': [true, 'allow-new'],
  'no-unused-variable': true,
  'no-use-before-declare': true,
  'no-var-keyword': true,
  'no-void-expression': [true, 'ignore-arrow-function-shorthand'],
  'prefer-conditional-expression': [true, 'check-else-if'],
  'prefer-object-spread': true,
  radix: true,
  'restrict-plus-operands': true,
  'strict-boolean-expressions': true,
  'strict-type-predicates': true,
  'switch-default': true,
  'triple-equals': true,
  'typeof-compare': true,
  'use-default-type-parameter': true,
  'use-isnan': true,
};

export default functionality;
