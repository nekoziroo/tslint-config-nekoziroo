/* tslint:disable */
import { Configuration } from 'tslint';

const possibleErrors: Configuration.RawRulesConfig = {
  'no-constant-condition': {
    options: [true, { checkLoops: false }],
    severity: 'warning',
  },
  'no-control-regex': true,
  'no-duplicate-case': true,
  'no-empty-character-class': true,
  'no-ex-assign': true,
  'no-extra-boolean-cast': true,
  'no-extra-semi': true,
  'no-inner-declarations': [true, 'functions'],
  'no-invalid-regexp': true,

  // Prioritize tslint no-irregular-whitespace
  'ter-no-irregular-whitespace': false,
  'no-regex-spaces': true,

  // Prioritize tslint no-sparse-arrays
  'ter-no-sparse-arrays': false,
  'no-unexpected-multiline': true,
  'valid-jsdoc': false,
  'valid-typeof': true,
};

const bestPractices: Configuration.RawRulesConfig = {
  'no-multi-spaces': true,
};

const nodejs: Configuration.RawRulesConfig = {
  'handle-callback-err': false,
};

const stylisticIssues: Configuration.RawRulesConfig = {
  'array-bracket-spacing': [true, 'never'],
  'block-spacing': [true, 'always'],
  'brace-style': [true, '1tbs', { allowSingleLine: true }],
  'ter-computed-property-spacing': [true, 'never'],
  'ter-func-call-spacing': [true, 'never'],
  'ter-indent': [
    true,
    2,
    {
      SwitchCase: 1,
      VariableDeclarator: 1,
      outerIIFEBody: 1,
      FunctionDeclaration: {
        parameters: 1,
        body: 1,
      },
      FunctionExpression: {
        parameters: 1,
        body: 1,
      },
      CallExpression: {
        arguments: 1,
      },
    },
  ],
  'ter-max-len': [
    true,
    100,
    {
      ignoreUrls: true,
      ignoreComments: false,
      ignoreRegExpLiterals: true,
      ignoreStrings: true,
      ignoreTemplateLiterals: true,
    },
  ],
  'ter-no-mixed-spaces-and-tabs': [{ type: 'spaces' }],
  'object-curly-spacing': [true, 'always'],
  'sort-imports': [
    true,
    { 'member-syntax-sort-order': ['none', 'all', 'multiple', 'single', 'alias'] },
  ],
  'space-in-parens': [true, 'never'],
  'ter-no-tabs': true,
};

const ecmaScript6: Configuration.RawRulesConfig = {
  'ter-arrow-body-style': [true, 'as-needed', { requireReturnForObjectLiteral: false }],
  'ter-arrow-parens': [true, 'as-needed', { requireForBlockBody: true }],
  'ter-arrow-spacing': [true, { before: true, after: true }],
  'ter-prefer-arrow-callback': false,
};

const tslintEslintRules: Configuration.RawRulesConfig = {
  ...possibleErrors,
  ...bestPractices,
  ...nodejs,
  ...stylisticIssues,
  ...ecmaScript6,
};

export default tslintEslintRules;
