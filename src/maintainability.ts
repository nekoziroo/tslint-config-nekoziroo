import { Configuration } from 'tslint';

const maintainability: Configuration.RawRulesConfig = {
  'cyclomatic-complexity': [false, 11],
  deprecation: true,
  eofline: true,

  // Prioritize tslint-eslint-rules ter-indent
  indent: [false, 'spaces', 2],
  'linebreak-style': [true, 'LF'],
  'max-classes-per-file': [true, 1],
  'max-file-line-count': [false, 300],

  // Prioritize tslint-eslint-rules ter-max-len
  'max-line-length': [false, { limit: 100 }],
  'no-default-export': false,
  'no-duplicate-imports': true,
  'no-mergeable-namespace': true,
  'no-require-imports': true,
  'object-literal-sort-keys': [false, 'ignore-case', 'match-declaration-order', 'shorthand-first'],
  'prefer-const': [true, { destructuring: 'any' }],
  'prefer-readonly': true,
  'trailing-comma': [true, { multiline: 'always', singleline: 'never' }],
};

export default maintainability;
