import { Configuration } from 'tslint';
import specific from './specific';
import functionality from './functionality';
import maintainability from './maintainability';
import style from './style';
import tslintEslintRules from './tslint-eslint-rules';

const config: Configuration.RawConfigFile = {
  extends: ['tslint-eslint-rules'],
  rules: {
    ...specific,
    ...functionality,
    ...maintainability,
    ...style,
    ...tslintEslintRules,
  },
};

export = config;
