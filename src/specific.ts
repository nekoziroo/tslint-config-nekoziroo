import { Configuration } from 'tslint';

const specific: Configuration.RawRulesConfig = {
  'adjacent-overload-signatures': true,
  'ban-types': [
    true,
    ['Boolean', "Use 'boolean' instead."],
    ['Number', "Use 'number' instead."],
    ['String', "use 'string' instead."],
    ['Object', "Use 'object' instead."],
  ],
  'member-access': [true, 'check-accessor', 'check-constructor', 'check-parameter-property'],
  'member-ordering': [true, { order: 'fields-first' }],
  'no-any': true,
  'no-empty-interface': true,
  'no-import-side-effect': true,
  'no-inferrable-types': [true, 'ignore-params', 'ignore-properties'],
  'no-internal-module': true,
  'no-magic-numbers': false,
  'no-namespace': [true, 'allow-declarations'],
  'no-non-null-assertion': true,
  'no-parameter-reassignment': true,
  'no-reference': true,
  'no-unnecessary-type-assertion': true,
  'no-var-requires': true,
  'only-arrow-functions': [true, 'allow-declarations', 'allow-named-functions'],
  'prefer-for-of': true,
  'promise-function-async': true,
  typedef: [true, 'call-signature', 'parameter', 'property-declaration'],
  'typedef-whitespace': [
    true,
    {
      'call-signature': 'nospace',
      'index-signature': 'nospace',
      parameter: 'nospace',
      'property-declaration': 'nospace',
      'variable-declaration': 'nospace',
    },
    {
      'call-signature': 'onespace',
      'index-signature': 'onespace',
      parameter: 'onespace',
      'property-declaration': 'onespace',
      'variable-declaration': 'onespace',
    },
  ],
  'unified-signatures': true,
};

export default specific;
