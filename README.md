# tslint-config-nekoziroo

[![MIT License](https://img.shields.io/badge/License-MIT-yellow.svg)](https://github.com/nekoziroo/tslint-config-nekoziroo/blob/master/LICENSE)

[tslint](https://github.com/palantir/tslint) config for nekoziroo

## Installation

Install with npm:

```bash
npm install --save-dev tslint-config-nekoziroo
```

Install with yarn:

```bash
yarn add --dev tslint-config-nekoziroo
```

## Usage

tslint.json:

```json
{
  "extends": "tslint-config-nekoziroo"
}
```

tslint.yaml:

```yaml
extends: tslint-config-nekoziroo
```
