const cac = require('cac');
const fs = require('fs');
const { promisify } = require('util');

const cli = cac();

const createDirectory = path => {
  fs.mkdirSync(path);
};

const createFile = async (path, data = '') => {
  await promisify(fs.writeFile)(path, data);
};

const indexData = (dir, rule, plugin = false) => {
  if (plugin) {
    return `const config = require('../../../lib/${dir}');

module.exports = {
  extends: ['${plugin}'],
  rules: {
    '${rule}': config.default['${rule}'],
  },
};
`;
  }
  return `const config = require('../../../lib/${dir}');

module.exports = {
  rules: {
    '${rule}': config.default['${rule}'],
  },
};
`;
};

const tslintData = {
  extends: './index.js',
}
;

const createTest = (input, flag) => {
  if (!flag.dir) {
    console.log('--dir option is required');
    return;
  }
  const test = 'test';
  if (flag.plugin) {
    input.forEach(rule => {
      createDirectory(`${test}/${flag.dir}/${rule}`);
      createFile(`${test}/${flag.dir}/${rule}/index.js`, indexData(flag.dir, rule, flag.plugin));
      createFile(`${test}/${flag.dir}/${rule}/test.ts`);
      createFile(`${test}/${flag.dir}/${rule}/tslint.json`, `${JSON.stringify(tslintData, null, 2)}\n`);
    });
    return;
  }
  input.forEach(rule => {
    createDirectory(`${test}/${flag.dir}/${rule}`);
    createFile(`${test}/${flag.dir}/${rule}/index.js`, indexData(flag.dir, rule));
    createFile(`${test}/${flag.dir}/${rule}/test.ts`);
    createFile(`${test}/${flag.dir}/${rule}/tslint.json`, `${JSON.stringify(tslintData, null, 2)}\n`);
  });
};

const testCommand = cli.command('*', { desc: 'command for create test' }, createTest);

testCommand.option('dir');

cli.parse();
