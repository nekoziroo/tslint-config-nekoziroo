const config = require('../../../lib/functionality');

module.exports = {
  rules: {
    'no-null-keyword': config.default['no-null-keyword'],
  },
};
