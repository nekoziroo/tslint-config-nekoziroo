const config = require('../../../lib/functionality');

module.exports = {
  rules: {
    'no-empty': config.default['no-empty'],
  },
};
