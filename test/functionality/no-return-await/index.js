const config = require('../../../lib/functionality');

module.exports = {
  rules: {
    'no-return-await': config.default['no-return-await'],
  },
};
