const config = require('../../../lib/functionality');

module.exports = {
  rules: {
    'no-unnecessary-class': config.default['no-unnecessary-class'],
  },
};
