const config = require('../../../lib/functionality');

module.exports = {
  rules: {
    'no-debugger': config.default['no-debugger'],
  },
};
