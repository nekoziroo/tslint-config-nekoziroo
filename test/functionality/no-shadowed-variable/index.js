const config = require('../../../lib/functionality');

module.exports = {
  rules: {
    'no-shadowed-variable': config.default['no-shadowed-variable'],
  },
};
