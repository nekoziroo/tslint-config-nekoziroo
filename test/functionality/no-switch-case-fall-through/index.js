const config = require('../../../lib/functionality');

module.exports = {
  rules: {
    'no-switch-case-fall-through': config.default['no-switch-case-fall-through'],
  },
};
