const config = require('../../../lib/functionality');

module.exports = {
  rules: {
    'no-object-literal-type-assertion': config.default['no-object-literal-type-assertion'],
  },
};
