const config = require('../../../lib/functionality');

module.exports = {
  rules: {
    'no-unused-expression': config.default['no-unused-expression'],
  },
};
