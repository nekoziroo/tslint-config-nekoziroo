const config = require('../../../lib/functionality');

module.exports = {
  rules: {
    'triple-equals': config.default['triple-equals'],
  },
};
