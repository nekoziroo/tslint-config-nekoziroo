const config = require('../../../lib/functionality');

module.exports = {
  rules: {
    'no-unsafe-finally': config.default['no-unsafe-finally'],
  },
};
