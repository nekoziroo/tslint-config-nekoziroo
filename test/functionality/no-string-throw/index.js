const config = require('../../../lib/functionality');

module.exports = {
  rules: {
    'no-string-throw': config.default['no-string-throw'],
  },
};
