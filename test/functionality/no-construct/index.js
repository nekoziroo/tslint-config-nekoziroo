const config = require('../../../lib/functionality');

module.exports = {
  rules: {
    'no-construct': config.default['no-construct'],
  },
};
