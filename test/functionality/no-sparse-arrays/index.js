const config = require('../../../lib/functionality');

module.exports = {
  rules: {
    'no-sparse-arrays': config.default['no-sparse-arrays'],
  },
};
