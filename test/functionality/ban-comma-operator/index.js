const config = require('../../../lib/functionality');

module.exports = {
  rules: {
    'ban-comma-operator': config.default['ban-comma-operator'],
  },
};
