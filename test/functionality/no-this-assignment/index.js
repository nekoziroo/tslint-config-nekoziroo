const config = require('../../../lib/functionality');

module.exports = {
  rules: {
    'no-this-assignment': config.default['no-this-assignment'],
  },
};
