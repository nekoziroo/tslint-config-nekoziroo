const config = require('../../../lib/functionality');

module.exports = {
  rules: {
    'no-string-literal': config.default['no-string-literal'],
  },
};
