const config = require('../../../lib/functionality');

module.exports = {
  rules: {
    'label-position': config.default['label-position'],
  },
};
