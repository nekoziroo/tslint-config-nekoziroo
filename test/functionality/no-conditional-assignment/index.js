const config = require('../../../lib/functionality');

module.exports = {
  rules: {
    'no-conditional-assignment': config.default['no-conditional-assignment'],
  },
};
