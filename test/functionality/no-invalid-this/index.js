const config = require('../../../lib/functionality');

module.exports = {
  rules: {
    'no-invalid-this': config.default['no-invalid-this'],
  },
};
