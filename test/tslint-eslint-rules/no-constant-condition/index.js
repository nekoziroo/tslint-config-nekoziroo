const config = require('../../../lib/tslint-eslint-rules');

module.exports = {
  extends: ['tslint-eslint-rules'],
  rules: {
    'no-constant-condition': config.default['no-constant-condition'],
  },
};
