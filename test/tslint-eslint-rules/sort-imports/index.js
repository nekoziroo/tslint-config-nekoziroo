const config = require('../../../lib/tslint-eslint-rules');

module.exports = {
  extends: ['tslint-eslint-rules'],
  rules: {
    'sort-imports': config.default['sort-imports'],
  },
};
