const config = require('../../../lib/style');

module.exports = {
  rules: {
    'arrow-parens': config.default['arrow-parens'],
  },
};
