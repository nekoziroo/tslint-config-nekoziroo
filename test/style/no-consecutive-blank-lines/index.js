const config = require('../../../lib/style');

module.exports = {
  rules: {
    'no-consecutive-blank-lines': config.default['no-consecutive-blank-lines'],
  },
};
