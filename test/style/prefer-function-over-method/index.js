const config = require('../../../lib/style');

module.exports = {
  rules: {
    'prefer-function-over-method': config.default['prefer-function-over-method'],
  },
};
