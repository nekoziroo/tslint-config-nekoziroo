const config = require('../../../lib/style');

module.exports = {
  rules: {
    'arrow-return-shorthand': config.default['arrow-return-shorthand'],
  },
};
