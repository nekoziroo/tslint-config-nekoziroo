const config = require('../../../lib/style');

module.exports = {
  rules: {
    'variable-name': config.default['variable-name'],
  },
};
