const config = require('../../../lib/style');

module.exports = {
  rules: {
    'object-literal-key-quotes': config.default['object-literal-key-quotes'],
  },
};
