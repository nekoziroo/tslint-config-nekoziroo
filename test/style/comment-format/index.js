const config = require('../../../lib/style');

module.exports = {
  rules: {
    'comment-format': config.default['comment-format'],
  },
};
