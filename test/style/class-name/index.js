const config = require('../../../lib/style');

module.exports = {
  rules: {
    'class-name': config.default['class-name'],
  },
};
