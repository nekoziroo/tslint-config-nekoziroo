const config = require('../../../lib/style');

module.exports = {
  rules: {
    'prefer-template': config.default['prefer-template'],
  },
};
