const config = require('../../../lib/style');

module.exports = {
  rules: {
    'binary-expression-operand-order': config.default['binary-expression-operand-order'],
  },
};
