const config = require('../../../lib/style');

module.exports = {
  rules: {
    'no-unnecessary-initializer': config.default['no-unnecessary-initializer'],
  },
};
