const config = require('../../../lib/style');

module.exports = {
  rules: {
    'interface-name': config.default['interface-name'],
  },
};
