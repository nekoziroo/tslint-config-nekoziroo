const config = require('../../../lib/style');

module.exports = {
  rules: {
    'switch-final-break': config.default['switch-final-break'],
  },
};
