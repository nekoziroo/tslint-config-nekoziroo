const foo = 'foo';

const bad = {
  foo: foo,
}

const good = {
  foo,
}
