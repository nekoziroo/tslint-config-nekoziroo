const config = require('../../../lib/style');

module.exports = {
  rules: {
    'object-literal-shorthand': config.default['object-literal-shorthand'],
  },
};
