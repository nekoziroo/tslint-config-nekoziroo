interface Bad {
  foo: () => void;
}

interface Good {
  foo(): void;
}
