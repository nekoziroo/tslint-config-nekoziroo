const config = require('../../../lib/style');

module.exports = {
  rules: {
    'prefer-method-signature': config.default['prefer-method-signature'],
  },
};
