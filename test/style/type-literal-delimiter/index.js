const config = require('../../../lib/style');

module.exports = {
  rules: {
    'type-literal-delimiter': config.default['type-literal-delimiter'],
  },
};
