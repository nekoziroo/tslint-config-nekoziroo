const config = require('../../../lib/style');

module.exports = {
  rules: {
    'no-unnecessary-callback-wrapper': config.default['no-unnecessary-callback-wrapper'],
  },
};
