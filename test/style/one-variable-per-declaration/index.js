const config = require('../../../lib/style');

module.exports = {
  rules: {
    'one-variable-per-declaration': config.default['one-variable-per-declaration'],
  },
};
