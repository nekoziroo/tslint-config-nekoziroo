const config = require('../../../lib/style');

module.exports = {
  rules: {
    'no-parameter-properties': config.default['no-parameter-properties'],
  },
};
