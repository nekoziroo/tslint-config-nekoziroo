const config = require('../../../lib/style');

module.exports = {
  rules: {
    'no-boolean-literal-compare': config.default['no-boolean-literal-compare'],
  },
};
