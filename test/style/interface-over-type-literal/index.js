const config = require('../../../lib/style');

module.exports = {
  rules: {
    'interface-over-type-literal': config.default['interface-over-type-literal'],
  },
};
