const config = require('../../../lib/style');

module.exports = {
  rules: {
    'space-before-function-paren': config.default['space-before-function-paren'],
  },
};
