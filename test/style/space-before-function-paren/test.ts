// anonymous
function () {}

// named
function named() {}

// async arrow
async () => {};

// constructor & method
class Foo {
  constructor() {}
  method() {}
}
