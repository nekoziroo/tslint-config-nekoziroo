const config = require('../../../lib/style');

module.exports = {
  rules: {
    'no-angle-bracket-type-assertion': config.default['no-angle-bracket-type-assertion'],
  },
};
