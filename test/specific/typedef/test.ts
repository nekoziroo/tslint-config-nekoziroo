// call-signature & parameter
function callSignature(parameter: string): void {}

// property-declaration
interface PropertyDeclaration {
  foo: string;
}
