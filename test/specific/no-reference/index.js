const config = require('../../../lib/specific');

module.exports = {
  rules: {
    'no-reference': config.default['no-reference'],
  },
};
