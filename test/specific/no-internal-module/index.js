const config = require('../../../lib/specific');

module.exports = {
  rules: {
    'no-internal-module': config.default['no-internal-module'],
  },
};
