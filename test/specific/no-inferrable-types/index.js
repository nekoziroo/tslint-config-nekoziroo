const config = require('../../../lib/specific');

module.exports = {
  rules: {
    'no-inferrable-types': config.default['no-inferrable-types'],
  },
};
