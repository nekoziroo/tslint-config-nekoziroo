const config = require('../../../lib/specific');

module.exports = {
  rules: {
    'prefer-for-of': config.default['prefer-for-of'],
  },
};
