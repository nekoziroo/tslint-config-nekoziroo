const config = require('../../../lib/specific');

module.exports = {
  rules: {
    'unified-signatures': config.default['unified-signatures'],
  },
};
