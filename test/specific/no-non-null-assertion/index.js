const config = require('../../../lib/specific');

module.exports = {
  rules: {
    'no-non-null-assertion': config.default['no-non-null-assertion'],
  },
};
