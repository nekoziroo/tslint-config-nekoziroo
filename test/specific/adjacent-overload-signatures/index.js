const config = require('../../../lib/specific');

module.exports = {
  rules: {
    'adjacent-overload-signatures': config.default['adjacent-overload-signatures'],
  },
};
