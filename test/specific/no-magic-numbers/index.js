const config = require('../../../lib/specific');

module.exports = {
  rules: {
    'no-magic-numbers': config.default['no-magic-numbers'],
  },
};
