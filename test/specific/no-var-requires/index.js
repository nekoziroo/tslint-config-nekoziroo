const config = require('../../../lib/specific');

module.exports = {
  rules: {
    'no-var-requires': config.default['no-var-requires'],
  },
};
