const config = require('../../../lib/specific');

module.exports = {
  rules: {
    'typedef-whitespace': config.default['typedef-whitespace'],
  },
};
