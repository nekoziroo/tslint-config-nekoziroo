const config = require('../../../lib/specific');

module.exports = {
  rules: {
    'no-parameter-reassignment': config.default['no-parameter-reassignment'],
  },
};
