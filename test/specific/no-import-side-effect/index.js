const config = require('../../../lib/specific');

module.exports = {
  rules: {
    'no-import-side-effect': config.default['no-import-side-effect'],
  },
};
