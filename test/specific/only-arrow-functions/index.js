const config = require('../../../lib/specific');

module.exports = {
  rules: {
    'only-arrow-functions': config.default['only-arrow-functions'],
  },
};
