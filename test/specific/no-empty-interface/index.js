const config = require('../../../lib/specific');

module.exports = {
  rules: {
    'no-empty-interface': config.default['no-empty-interface'],
  },
};
