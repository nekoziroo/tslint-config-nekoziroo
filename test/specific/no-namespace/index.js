const config = require('../../../lib/specific');

module.exports = {
  rules: {
    'no-namespace': config.default['no-namespace'],
  },
};
