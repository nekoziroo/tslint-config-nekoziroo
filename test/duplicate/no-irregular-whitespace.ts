function thing1() /*<NBSP>*/{
  return 'test';
}

function thing2( /*<NBSP>*/): string {
  return 'test';
}

function thing3 /*<NBSP>*/(): string {
  return 'test';
}

function thing᠎4/*<MVS>*/(): string {
  return 'test';
}

function thing5(): string {
  return 'test'; /*<ENSP>*/
}

function thing6(): string {
  return 'test'; /*<NBSP>*/
}
