const foo = x => x * x;

const bar = (x) => {
  const one = 1;
  return x * one;
};

const baz = x => { x * x };
