const config = require('../../../lib/maintainability');

module.exports = {
  rules: {
    'no-duplicate-imports': config.default['no-duplicate-imports'],
  },
};
