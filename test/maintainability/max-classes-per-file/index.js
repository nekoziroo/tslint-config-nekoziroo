const config = require('../../../lib/maintainability');

module.exports = {
  rules: {
    'max-classes-per-file': config.default['max-classes-per-file'],
  },
};
