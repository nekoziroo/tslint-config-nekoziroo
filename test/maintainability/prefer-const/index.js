const config = require('../../../lib/maintainability');

module.exports = {
  rules: {
    'prefer-const': config.default['prefer-const'],
  },
};
