const config = require('../../../lib/maintainability');

module.exports = {
  rules: {
    'linebreak-style': config.default['linebreak-style'],
  },
};
