const config = require('../../../lib/maintainability');

module.exports = {
  rules: {
    'no-default-export': config.default['no-default-export'],
  },
};
