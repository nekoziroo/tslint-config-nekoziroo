const config = require('../../../lib/maintainability');

module.exports = {
  rules: {
    'trailing-comma': config.default['trailing-comma'],
  },
};
